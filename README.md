﻿# **Resposta Desafio Backend.** #

O sistema foi desenvolvido com Django 2.0

Para rodar o sistema em Linux:

Instalar Python3.4, 3.5 ou 3.6. (Testado com 3.6)

Instalar o pip3 (pip para python3)

Instalar o Django (pip3 install Django)

Abrir o terminal na pasta raíz do projeto, onde está o arquivo manage.py,
e executar o servidor. 
	(python3 manage.py runserver)

Acessar 127.0.0.1:8000 ou localhost:8000 no navegador.

Para fazer login no sistema: Usuário = admin; Senha = senha123

